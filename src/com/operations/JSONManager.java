package com.operations;

import com.extensions.AlreadyExistsException;
import com.extensions.NotFoundException;
import com.types.Note;
import com.types.TodoList;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;

public class JSONManager {
    public static void loadJSON(TodoList todoList, String path){
        try{
            JSONToMap(todoList,path);
        }catch (NotFoundException e){
            System.out.println(e);
        }
    }

    public static void JSONToMap(TodoList todoList, String path) throws NotFoundException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(new File(path));

            JsonNode notes = rootNode.get(todoList.getName());

            if (notes == null){
                throw new NotFoundException("List",todoList.getName());
            }

            for (JsonNode node : notes) {
                todoList.addNote(new Note(
                        node.get("Name").asText(),
                        node.get("Summary").asText(),
                        node.get("Description").asText(),
                        node.get("State").asText()
                ));
            }
        } catch (IOException e) {
            System.out.println("Organizer is empty!"); // TODO: handle if json is empty -> print is empty and return to menu
            throw new NotFoundException();
        }
    }

    public static void MapToJSON (TodoList todoList, String path) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        try {
            rootNode = mapper.readTree(new File(path));
        }catch (IOException e) {
            rootNode = mapper.createObjectNode();
        }
        ArrayNode notes = ((ObjectNode) rootNode).putArray(todoList.getName());

        Enumeration<Note> values = todoList.getNotes().elements();

        while(values.hasMoreElements()){
            JsonNode node = mapper.createObjectNode();
            Note note = values.nextElement();
            ((ObjectNode)node).put("Name",note.getName());
            ((ObjectNode)node).put("Summary",note.getSummary());
            ((ObjectNode)node).put("Description",note.getDescription());
            ((ObjectNode)node).put("State",note.stateToString());
            notes.add(node);
        }
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path), rootNode);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void createJSONNode(String name, String path){
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode;
        try {
            rootNode = mapper.readTree(new File(path));
        }catch (IOException e) {
            rootNode = mapper.createObjectNode();
        }
        try {
            JsonNode node = rootNode.get(name);
            if (node != null) throw new AlreadyExistsException("List",name);

            ((ObjectNode)rootNode).putArray(name);
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path),rootNode);
            System.out.println("List created successfully!");
        }catch (AlreadyExistsException e){
            System.out.println(e);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void removeJSONNode(String name, String path){
        try {
            ObjectMapper mapper = new ObjectMapper();

            JsonNode rootNode = mapper.readTree(new File(path));

            JsonNode node = rootNode.get(name);
            if (node == null) throw new NotFoundException("List",name);

            ((ObjectNode)rootNode).remove(name);
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(path),rootNode);
            System.out.println("List \"" + name + "\" successfully removed!");
        }catch (IOException e) {
            System.out.println("Organizer is empty!");
        }catch (NotFoundException e){
            System.out.println(e);
        }
    }

    public static void returnList(String path){
        try {
            ObjectMapper mapper = new ObjectMapper();

            JsonNode rootNode = mapper.readTree(new File(path));
            Iterator<String> names = rootNode.getFieldNames();
            while(names.hasNext()){
                System.out.println("- " + names.next());
            }
        }catch (IOException e) {
            System.out.println("Organizer is empty!");
        }
    }
}