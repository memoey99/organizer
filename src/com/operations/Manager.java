package com.operations;

import com.extensions.AlreadyExistsException;
import com.extensions.EmptyCollectionException;
import com.extensions.NotFoundException;
import com.types.Note;
import com.types.TodoList;

public class Manager {
    public interface Function{
        void operation() throws NotFoundException, AlreadyExistsException, EmptyCollectionException;
    }

    private static void operateOnNote(TodoList todoList, String path, Function func){
        JSONManager.loadJSON(todoList,path);
        try {
            func.operation();
            JSONManager.MapToJSON(todoList, path);
        }catch (NotFoundException e){
            System.out.println(e);
        }catch (AlreadyExistsException e) {
            System.out.println(e);
        }catch (EmptyCollectionException e){
            System.out.println(e);
        }
    }

    public static void addNote(TodoList todoList, String path, Note note){
        operateOnNote(todoList,path,()->{
            if (todoList.getNotes().containsKey(note.getName()))
                throw new AlreadyExistsException("Note",note.getName());
            todoList.addNote(note);
        });
    }

    public static void removeNote(TodoList todoList, String path, String name){
        operateOnNote(todoList,path,()->{
            if (!todoList.getNotes().containsKey(name))
                throw new NotFoundException("Note",name);
            todoList.removeNote(name);
        });
    }

    public static void completeNote(TodoList todoList, String path, String name){
        operateOnNote(todoList,path,()-> {
            if (!todoList.getNotes().containsKey(name))
                throw new NotFoundException("Note",name);
            todoList.complete(name);
        });
    }

    public static void resetNoteProgress(TodoList todoList, String path, String name){
        operateOnNote(todoList,path,()-> {
            if (!todoList.getNotes().containsKey(name))
                throw new NotFoundException("Note",name);
            todoList.resetNoteState(name);
        });
    }

    public static void resetProgress(TodoList todoList, String path){
        operateOnNote(todoList,path,()->{
            if (todoList.getNotes().size() == 0)
                throw new EmptyCollectionException("List");
            todoList.resetProgress();
        });
    }
}
