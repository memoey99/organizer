package com.tests;

import com.types.Note;
import com.types.TodoList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TodoListTest {

    @Test
    void countProgress() {
        TodoList todoList = new TodoList();
        assertEquals(0,todoList.countProgress());
    }
    @Test
    void countProgress2() {
        TodoList todoList = new TodoList();
        Note note1 = new Note("1","","","completed");
        Note note2 = new Note("2","","","uncompleted");
        Note note3 = new Note("3","","","uncompleted");
        Note note4 = new Note("4","","","completed");
        todoList.addNote(note1);
        todoList.addNote(note2);
        todoList.addNote(note3);
        todoList.addNote(note4);
        assertEquals(50,todoList.countProgress());
    }
    @Test
    void addNote() {
        TodoList todoList = new TodoList();
        Note note1 = new Note("1","","","completed");
        Note note2 = new Note("1","","","uncompleted"); //should not add
        Note note3 = new Note("2","","","uncompleted");
        Note note4 = new Note("2","","","completed"); // should not add
        todoList.addNote(note1);
        todoList.addNote(note2);
        todoList.addNote(note3);
        todoList.addNote(note4);
        assertEquals(2,todoList.getNotes().size());
    }
}