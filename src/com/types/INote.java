package com.types;

public interface INote {
    String getName();
    void setName(String name);
    String getSummary();
    void setSummary(String summary);
    String getDescription();
    void setDescription(String description);
    String toString();
}
