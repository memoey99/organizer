package com.types;

import java.util.Hashtable;

public interface IDirectory {
    Hashtable<String, Note> getNotes();
    void setNotes(Hashtable<String, Note> notes);
    void addNote(Note note);
    void removeNote(String key);
    String toString();
}
