package com.types;

import java.util.Hashtable;
import java.util.Iterator;

public class TodoList implements IDirectory{
    private Hashtable<String,Note> notes;
    private String name = "none";
    public TodoList() {
        notes = new Hashtable<String,Note>();
    }

    public TodoList(Hashtable<String,Note> notes) {
        this.notes = notes;
    }
    public TodoList(String name) { this.name = name; notes = new Hashtable<String,Note>(); }

    public Hashtable<String, Note> getNotes() {
        return notes;
    }

    public void setNotes(Hashtable<String, Note> notes) {
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addNote(Note note){
        this.notes.put(note.getName(),note);
    }

    public void removeNote(String key) { this.notes.remove(key); }

    public String toString(){
        if (notes.size() == 0) return "List is empty";
        String notesDescription = "";
        Iterator<String> itr = notes.keySet().iterator();
        while(itr.hasNext()){
            notesDescription += notes.get(itr.next());
        }
        return notesDescription;
    }

    public void complete(String key) {
        this.notes.get(key).complete();
    }

    public void resetNoteState(String key) {
        this.notes.get(key).resetState();
    }

    public void resetProgress(){
        Iterator<String> itr = notes.keySet().iterator();
        while(itr.hasNext()){
            this.notes.get(itr.next()).resetState();
        }
    }

    public double countProgress() {
        // case if list is empty
        if (notes.size() == 0) return 0;
        // if list got one or more points(notes)
        Iterator<String> itr = notes.keySet().iterator();
        int counter = 0;
        while(itr.hasNext()){
            if (notes.get(itr.next()).getState()) counter++;
        }
        return (double)(100 * counter/notes.size());
    }
}
