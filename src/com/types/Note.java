package com.types;

public class Note implements INote {
    private String name = "none";
    private String summary = "none";
    private  String description = "none";
    private boolean state = false; // false - uncompleted, true - completed

    public Note() {}

    public Note(String name, String summary, String description, String state) {
        this.name = name;
        this.summary = summary;
        this.description = description;
        this.state = state.equals("completed");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getState() { return state; }
    public void complete(){ this.state = true; }

    public void resetState() { this.state = false; }

    public String stateToString() {
        return state ? "completed" : "uncompleted";
    }
    public String toString(){
        return "Name: " + name + "" +
                "\nSummary: " + summary + "" +
                "\nDescription: " + description + "" +
                "\nState: " + stateToString() + "\n\n";
    }
}
