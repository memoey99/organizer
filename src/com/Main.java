package com;

import com.extensions.NotFoundException;
import com.operations.JSONManager;
import com.operations.Manager;
import com.types.Note;
import com.types.TodoList;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String path = "./config/data.json";
        Scanner scanner = new Scanner(System.in);
        System.out.print(mainMenu());
        String name;
        main:
        while(true) {
            printPath(new String[]{"main"});
            switch (scanner.next()) {
                case "notes":
                    name = readLine("list name");
                    TodoList todoList = new TodoList(name);
                    try {
                        JSONManager.JSONToMap(todoList,path); }
                    catch (NotFoundException e){
                        System.out.println(e);
                        break;
                    }
                    System.out.println(noteInstruction());
                    notes_operations:
                    while (true) {
                        printPath(new String[]{"main","notes",todoList.getName()});
                        switch (scanner.next()) {
                            case "show":
                                System.out.println(todoList);
                                break;
                            case "add":
                                Note note = new Note();
                                note.setName(readLine("name"));
                                note.setSummary(readLine("summary"));
                                note.setDescription(readLine("description"));
                                Manager.addNote(todoList, path, note);
                                break;
                            case "delete":
                                name = readLine("note name");
                                Manager.removeNote(todoList, path, name);
                                break;
                            case "complete":
                                name = readLine("note name");
                                Manager.completeNote(todoList, path, name);
                                break;
                            case "undo":
                                name = readLine("note name");
                                Manager.resetNoteProgress(todoList, path, name);
                                break;
                            case "reset":
                                Manager.resetProgress(todoList, path);
                                break;
                            case "progress":
                                System.out.println(todoList.countProgress() + "%");
                                break;
                            case "back":
                                System.out.println(mainMenu());
                                break notes_operations;
                            case "quit":
                                break main;
                            case "help":
                                System.out.println(noteInstruction());
                                break;
                            default:
                                System.out.println("Command unknown!");
                                break;
                        }
                    }
                    break;
                case "list":
                    System.out.println(listInstruction());
                    lists_operations:
                    while(true){
                        printPath(new String[]{"main","list"});
                        switch(scanner.next()){
                            case "add":
                                name = readLine("list name");
                                JSONManager.createJSONNode(name,path);
                                break;
                            case "delete":
                                name = readLine("list name");
                                JSONManager.removeJSONNode(name,path);
                                break;
                            case "show":
                                JSONManager.returnList(path);
                                break;
                            case "help":
                                System.out.println(listInstruction());
                                break;
                            case "back":
                                System.out.println(mainMenu());
                                break lists_operations;
                            case "quit":
                                break main;
                            default:
                                System.out.println("Command unknown!");
                                break;
                        }
                    }
                    break;
                case "help":
                    System.out.println(mainMenu());
                    break;
                case "quit":
                    break main;
                default:
                    System.out.println("Command unknown!");
                    break;
            }
        }
        System.out.println("Closing...");
    }

    public static void printPath(String[] path){
        String result = "";
        for (String s : path){
            result += s + ">";
        }
        result += " Enter command: ";
        System.out.print(result);
    }

    public static String mainMenu(){
        return "Main menu - commands:\n" +
                "list   - list operations(create,delete list ...)\n" +
                "notes  - notes operations for chosen list(add,delete...)\n" +
                "help   - print commands\n" +
                "quit   - to close app\n";
    }

    public static String noteInstruction(){
        return "Note - commands: \n" +
                "show       - show all notes\n" +
                "add        - create new note and add it to list\n" +
                "delete     - delete note from list\n" +
                "help       - print instruction\n" +
                "complete   - set note as completed\n" +
                "undo       - reset note progress\n" +
                "reset      - reset all progress\n" +
                "progress   - print current progress\n" +
                "back       - back to main menu\n" +
                "quit       - to close app\n";
    }

    public  static String listInstruction(){
        return "List - commands:\n" +
                "add    - create new TODO list\n" +
                "delete - remove TODO list\n" +
                "show   - show all lists\n" +
                "help   - print commands\n" +
                "back   - back to main menu\n" +
                "quit   - close app\n";
    }

    public static String readLine(String name){
        System.out.print("Enter " + name + ": ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
