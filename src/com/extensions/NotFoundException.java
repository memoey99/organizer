package com.extensions;

public class NotFoundException extends Exception{
    String msg;

    public NotFoundException() {
        this.msg = "";
    }

    public NotFoundException(String type, String name) {
        this.msg = (type + " \"" + name + "\"do not exists!");
    }

    @Override
    public String toString () {
        return msg;
    }
}