package com.extensions;

public class AlreadyExistsException extends Exception{
    String msg;
    public AlreadyExistsException(String type, String name) {
        this.msg = (type + " \"" + name+ "\" already exists!");
    }

    @Override
    public String toString () {
        return msg;
    }
}
