package com.extensions;

public class EmptyCollectionException extends Exception{
    String msg;
    public EmptyCollectionException(String collectionName) {
        this.msg = (collectionName + "is empty!");
    }

    @Override
    public String toString () {
        return msg;
    }
}
